use std::sync::mpsc;
use std::thread;
use std::time::Duration;

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();

    let tx1 = mpsc::Sender::clone(&tx);
    let tx2 = mpsc::Sender::clone(&tx);
    let tx3 = mpsc::Sender::clone(&tx);

    let handle1 = thread::spawn(move || { 
        loop {
            let val = String::from("Hi from thread 1");
            tx1.send(val).unwrap();
            thread::sleep(Duration::from_millis(1000));
        }
    });
    let handle2 = thread::spawn(move || { 
        loop {
            let val = String::from("Hi from thread 2");
            tx2.send(val).unwrap();
            thread::sleep(Duration::from_millis(1000));
        }
    });
    let handle3 = thread::spawn(move || { 
        loop {
            let val = String::from("Hi from thread 3");
            tx3.send(val).unwrap();
            thread::sleep(Duration::from_millis(1000));
        }
    });
    
    for received in rx {
        println!("Got: {}", received);
    }

    handle1.join().unwrap();
    handle2.join().unwrap();
    handle3.join().unwrap();
}
